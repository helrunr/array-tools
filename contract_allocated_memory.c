#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char *contractAllocatedMemory(char *arr) {

    char *initial = arr;
    char *new = arr;

    /* Skip over leading blanks */
    while (*initial == ' ') {
        initial++;
    }

    /* Copy remaining characters to the beginning of the string */
    while (*initial) {
        *(new++) = *(initial++);
    }

    *new = 0;

    /* reallocate memory based on the new string */
    return (char*) realloc(arr, strlen(arr) + 1);
}

