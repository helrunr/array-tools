#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

int findSmallest(const int *list, const int sze) {

    int x = 0, first = INT_MAX;
    int rslt[sze];

    while (x < sze)
    {
        if (list[x] < first)
        {
            first = list[x];
            rslt[0] = first;
        }

        x++;
    }

    return *rslt;
}


