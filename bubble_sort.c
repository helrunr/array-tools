#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

/* Compare function for sorting */
int comp (const char* s1, const char* s2) {
    return strcmp(s1, s2);
}

/* Function that converts sorting candidates to all lower case */
char *lCase (const char *string) {
    char *tmp = (char*) malloc(strlen(string) + 1);
    char *start = tmp;

    while (*string != 0)
    {
        *tmp++ = tolower(*string++);
    }

    *tmp = 0;

    return start;
}

/*
 * Function for ignoring the case of sorting candidates.
 * By default the sorting algorithm will sort upper case candidates first
 * and then lower case candidates.
 */
int compIgnCase (const char *s1, const char * s2) {
    char *t1 = lCase(s1);
    char *t2 = lCase(s2);
    int result = strcmp(t1, t2);

    free(t1);
    free(t2);

    return result;
}

/*
 * Function pointer that will be used in defining the way sorting behaves
 * in the sort algorithm
 */
typedef int (*ptrOp)(const char*, const char*);

/*
 * This is the main sorting function. The third parameter is a pointer
 * to a function which is used to define a sorting mechanism.
 */
void sort(char *array[], int size, ptrOp procedure) {
    int swap = 1;

    while (swap)
    {
        swap = 0;

        for (int i = 0; i < size - 1; ++i)
        {
            /*
             * procedure is a pointer to a function that provides the logic
             * for sorting the elements based on case or not based on case.
             */
            if (procedure(array[i], array[i+1]) > 0)
            {
                swap = 1;
                char *tmp = array[i];
                array[i] = array [i+1];
                array[i + 1] = tmp;
            }
        }
    }
}

/* Simple function for displaying contents of the sorted array. */
void displaySort (char *members[], int size) {
    for (int i = 0; i < size; ++i)
    {
        printf("%s\n", members[i]);
    }
}


