#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

int findLargest(const int *list, const int sze) {

    int x = 0, first = 0;
    int rslt[sze];

    while (x < sze) {
        if (list[x] > first) {
            first = list[x];
            rslt[0] = first;
        }

        x++;
    }

    return *rslt;
}
